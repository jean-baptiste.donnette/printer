#!/bin/bash
set -x

echo 'Install brew'

echo "You need brew to continue this script, i'm going to install it. Please Press ENTER when the script is asking it, or the installation will fail."
if ! type "brew" > /dev/null; then
  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

if ! type "wget" > /dev/null; then
  brew install wget
fi

echo "Downloading printer requirement files"

wget http://nextcloud.nexcloud.devops.owkin.com/s/qp9ZsWNG4zwimGP/download -O XeroxPrintDriver_5.3.0_2118.dmg

echo 'Running printer install 1/2'
sudo hdiutil attach XeroxPrintDriver_5.3.0_2118.dmg


echo 'Running printer install 2/2'
sudo installer -package /Volumes/Xerox\ Print\ Driver\ 5.3.0/Xerox\ Print\ Driver\ 5.3.0.pkg -target /

echo "Setting up printer. 1/2"
lpadmin -p Xerox_NB -L "Xerox_NB" -E -v lpd://10.0.20.10/lp -o printer-is-shared=false -P "/Library/Printers/PPDs/Contents/Resources/Xerox VersaLink B7025.gz"

echo "Setting up printer. 2/2"
lpadmin -p Xerox_COULEUR -L "Xerox_COULEUR" -E -v lpd://10.0.20.10/lp -o printer-is-shared=false -P "/Library/Printers/PPDs/Contents/Resources/Xerox VersaLink C7020.gz"

echo "Done."
rm XeroxPrintDriver_5.3.0_2118.dmg